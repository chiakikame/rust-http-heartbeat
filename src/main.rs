extern crate rouille;
extern crate clap;
extern crate chrono;

use clap::{App, Arg};

fn main() {
    let matches = app().get_matches();
    let identity = matches.value_of("identity");

    if identity.is_none() {
        println!("Please specify identity.");
    } else {
        let identity = identity.unwrap().to_owned();
        let bind_target = matches.value_of("bind").unwrap();

        println!(
            "Heartbeat server binds to http://{} is starting",
            bind_target
        );

        rouille::start_server(bind_target, move |req| {
            let now = chrono::Utc::now();
            let remote_addr = req.remote_addr();
            rouille::Response::text(format!(
                "Timestamp\t{}\nMachineIdentity\t{}\nRemoteAddr\t{}\n",
                now.timestamp(),
                identity,
                remote_addr
            ))
        });
    }
}

fn app() -> App<'static, 'static> {
    let identity_arg = Arg::with_name("identity")
        .short("i")
        .long("identity")
        .takes_value(true)
        .value_name("IDENTITY")
        .required(true)
        .help("Identity of this machine");

    let binding_arg = Arg::with_name("bind")
        .short("b")
        .long("bind")
        .default_value("localhost:9988")
        .takes_value(true)
        .value_name("Bind target")
        .help("Ip and port for this server binding to, in ip:port format");

    App::new("echo-http").version("1.0").arg(identity_arg).arg(
        binding_arg,
    )
}
