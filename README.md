# Rust HTTP heartbeat server

A very simple HTTP heartbeat server in Rust programming language.

## License

This program is licensed under GNU-GPLv3.

## Usage

To build

```
$ cargo build --release
```

Then you may find the executable in `./target/release/rust-http-heartbeat`.
To run it:

```
$ cd ./target/release 
$ ./rust-http-heartbeat -i "Machine 1"
```

To listen to specific port

```
$ ./rust-http-heartbeat -i "Machine 1" -b localhost:38490
```

To listen to specific ip and port 

```
$ ./rust-http-heartbeat -i "Machine 1" -b 192.168.38.49:38490
```

After the server starts, you can navigate to the location indicated by
console message.

## Example session

```
$ ./target/debug/rust-http-heartbeat -i "Machine 1" &
[1] 12436
Heartbeat server binds to http://localhost:9988 is starting

$ curl http://localhost:9988                         
Timestamp       1509036530
MachineIdentity Machine 1
RemoteAddr      127.0.0.1:37026
```